package main

import (
	"log"
	"todo_template_go/config"
	"todo_template_go/internal/server"
	"todo_template_go/pkg/logger"
)

func main() {
	configPath := "./config/config-local"

	cfgFile, err := config.LoadConfig(configPath)
	if err != nil {
		log.Fatalf("LoadConfig: %v", err)
	}

	cfg, err := config.ParseConfig(cfgFile)
	if err != nil {
		log.Fatalf("ParseConfig: %v", err)
	}

	appLogger := logger.NewApiLogger(cfg)
	appLogger.InitLogger()

	// apm.DefaultTracer.SetLogger(appLogger)

	appLogger.Info("App Begin Starting")

	s := server.NewServer(appLogger, cfg)
	if err := s.Run(); err != nil {
		log.Fatal(err)
	}
}
