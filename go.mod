module todo_template_go

go 1.16

require (
	github.com/elastic/go-sysinfo v1.6.0 // indirect
	github.com/elastic/go-windows v1.0.1 // indirect
	github.com/go-playground/validator/v10 v10.4.1
	github.com/gorilla/mux v1.8.0
	github.com/jmoiron/sqlx v1.3.1
	github.com/opentracing/opentracing-go v1.2.0
	github.com/pkg/errors v0.9.1 // indirect
	github.com/prometheus/procfs v0.6.0 // indirect
	github.com/sirupsen/logrus v1.8.0
	github.com/spf13/viper v1.7.1
	go.elastic.co/apm v1.11.0
	go.elastic.co/apm/module/apmlogrus v1.11.0
	golang.org/x/sys v0.0.0-20210223212115-eede4237b368 // indirect
	howett.net/plist v0.0.0-20201203080718-1454fab16a06 // indirect
)
