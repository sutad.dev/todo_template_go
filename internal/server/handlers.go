package server

import (
	todoHttp "todo_template_go/internal/todo/delivery/http"
	todoUseCase "todo_template_go/internal/todo/usecase"

	"github.com/gorilla/mux"
)

func (s *Server) MapHandlers(r *mux.Router) error {
	todoUC := todoUseCase.NewTodoUseCase(s.logger)
	todoHandlers := todoHttp.NewTodoHandlers(todoUC, s.logger)

	v1 := r.PathPrefix("/v1").Subrouter()

	todo := v1.PathPrefix("/todo").Subrouter()

	todoHttp.MapTodoRoutes(todo, todoHandlers)

	return nil
}
