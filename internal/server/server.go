package server

import (
	"log"
	"net/http"
	"todo_template_go/config"
	"todo_template_go/pkg/logger"

	"github.com/gorilla/mux"
)

type Server struct {
	mux    *mux.Router
	cfg    *config.Config
	logger logger.Logger
}

func NewServer(logger logger.Logger, cfg *config.Config) *Server {
	return &Server{
		mux:    mux.NewRouter(),
		cfg:    cfg,
		logger: logger,
	}
}

func (s *Server) Run() error {
	if err := s.MapHandlers(s.mux); err != nil {
		return err
	}

	s.logger.Infof("Server is listening no PORT%s", s.cfg.Server.Port)
	log.Fatal(http.ListenAndServe(s.cfg.Server.Port, s.mux))
	return nil
}
