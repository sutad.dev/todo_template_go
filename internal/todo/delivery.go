package todo

import "net/http"

type Handlers interface {
	CreateTodo(w http.ResponseWriter, r *http.Request)
	GetTodo(w http.ResponseWriter, r *http.Request)
}
