package http

import (
	"encoding/json"
	"log"
	"net/http"
	"todo_template_go/internal/todo"
	"todo_template_go/pkg/logger"

	"go.elastic.co/apm"
)

type todoHandlers struct {
	todoUC todo.UseCase
	logger logger.Logger
}

func NewTodoHandlers(todoUC todo.UseCase, logger logger.Logger) todo.Handlers {
	return &todoHandlers{
		todoUC: todoUC,
		logger: logger,
	}
}

func (h *todoHandlers) CreateTodo(w http.ResponseWriter, r *http.Request) {

}

func (h *todoHandlers) GetTodo(w http.ResponseWriter, r *http.Request) {
	span, ctx := apm.StartSpan(r.Context(), "todoHandlers.GetTodo", "custom")
	defer span.End()

	todo, err := h.todoUC.GetTodo(ctx)
	if err != nil {
		log.Println(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(todo)
}
