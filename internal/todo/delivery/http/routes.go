package http

import (
	"todo_template_go/internal/todo"

	"github.com/gorilla/mux"
)

// MapTodoRoutes ...
func MapTodoRoutes(r *mux.Router, h todo.Handlers) {
	r.HandleFunc("", h.CreateTodo).Methods("POST")
	r.HandleFunc("", h.GetTodo).Methods("GET")

}
