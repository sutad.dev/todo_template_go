package todo

import (
	"context"
	"todo_template_go/internal/models"
)

type UseCase interface {
	GetTodo(ctx context.Context) (*models.Todo, error)
}
