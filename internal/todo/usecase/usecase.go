package usecase

import (
	"context"
	"todo_template_go/internal/models"
	"todo_template_go/internal/todo"
	"todo_template_go/pkg/logger"

	"go.elastic.co/apm"
)

type todoUC struct {
	logger logger.Logger
}

func NewTodoUseCase(logger logger.Logger) todo.UseCase {
	return &todoUC{
		logger: logger,
	}
}

func (u *todoUC) GetTodo(ctx context.Context) (*models.Todo, error) {
	span, ctx := apm.StartSpan(ctx, "todoUC.GetTodo", "custom")
	defer span.End()

	todo := &models.Todo{
		Name:  "TEST-Todo",
		LName: "Hello",
	}

	u.logger.Infof("Name: %s, Last Name: %s", todo.Name, todo.LName)

	return todo, nil
}
