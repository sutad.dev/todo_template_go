package logger

import (
	"os"
	"todo_template_go/config"

	logrus "github.com/sirupsen/logrus"
	"go.elastic.co/apm/module/apmlogrus"
)

type Logger interface {
	InitLogger()
	Debug(args ...interface{})
	Debugf(template string, args ...interface{})
	Info(args ...interface{})
	Infof(template string, args ...interface{})
	Warn(args ...interface{})
	Warnf(template string, args ...interface{})
	Error(args ...interface{})
	Errorf(template string, args ...interface{})
	Fatal(args ...interface{})
	Fatalf(template string, args ...interface{})
}

type apiLogger struct {
	cfg          *config.Config
	logrusLogger *logrus.Logger
}

func NewApiLogger(cfg *config.Config) *apiLogger {
	return &apiLogger{
		cfg: cfg,
	}
}

func (l *apiLogger) InitLogger() {
	logger := logrus.New()

	logger.SetFormatter(&logrus.TextFormatter{
		DisableColors: false,
		FullTimestamp: true,
	})
	logger.SetOutput(os.Stdout)
	logger.SetLevel(logrus.DebugLevel)

	logger.AddHook(&apmlogrus.Hook{})

	l.logrusLogger = logger
}

func (l *apiLogger) Debug(args ...interface{}) {
	l.logrusLogger.Debug(args...)
}

func (l *apiLogger) Debugf(template string, args ...interface{}) {
	l.logrusLogger.Debugf(template, args...)
}

func (l *apiLogger) Info(args ...interface{}) {
	l.logrusLogger.Info(args...)
}

func (l *apiLogger) Infof(template string, args ...interface{}) {
	l.logrusLogger.Infof(template, args...)
}

func (l *apiLogger) Warn(args ...interface{}) {
	l.logrusLogger.Warn(args...)
}

func (l *apiLogger) Warnf(template string, args ...interface{}) {
	l.logrusLogger.Warnf(template, args...)
}

func (l *apiLogger) Error(args ...interface{}) {
	l.logrusLogger.Error(args...)
}

func (l *apiLogger) Errorf(template string, args ...interface{}) {
	l.logrusLogger.Errorf(template, args...)
}

func (l *apiLogger) Panic(args ...interface{}) {
	l.logrusLogger.Panic(args...)
}

func (l *apiLogger) Panicf(template string, args ...interface{}) {
	l.logrusLogger.Panicf(template, args...)
}

func (l *apiLogger) Fatal(args ...interface{}) {
	l.logrusLogger.Fatal(args...)
}

func (l *apiLogger) Fatalf(template string, args ...interface{}) {
	l.logrusLogger.Fatalf(template, args...)
}
